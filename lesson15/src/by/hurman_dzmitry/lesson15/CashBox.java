package by.hurman_dzmitry.lesson15;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class CashBox {
    Customers customers;
    ReentrantLock locker = new ReentrantLock();
    Condition condition = locker.newCondition();

    public ReentrantLock getLocker() {
        return locker;
    }

    public Condition getCondition() {
        return condition;
    }

    public CashBox(Customers customers) {
        this.customers = customers;
    }

    public void customersOut() {
        locker.lock();
        while (customers.getCustomerList().size() < 1) {
            try {
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Ушел " + customers.getCustomerList().get(customers.getCustomerList().size() - 1) + " in " + Thread.currentThread().getName());
        customers.getCustomerList().remove(0);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            locker.unlock();
        }
    }
}