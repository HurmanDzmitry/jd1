package by.hurman_dzmitry.lesson15;

public class Store {
    Customers customers;
    CashBox cashBox;

    public Store(Customers customers) {
        this.customers = customers;
    }

    public void customersIn() {
        cashBox.locker.lock();
        customers.getCustomerList().add(new Customer((int) (Math.random() * 100)));
        System.out.println("Пришел " + customers.getCustomerList().get(customers.getCustomerList().size() - 1));
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cashBox.condition.signalAll();
        cashBox.locker.unlock();
    }
}
