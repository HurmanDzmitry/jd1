package by.hurman_dzmitry.lesson15;

public class CustomersThread implements Runnable {
    Store store;

    public CustomersThread(Store store) {
        this.store = store;
    }

    @Override
    public void run() {
        for (int i = 0; i < 7; i++) {
            store.customersIn();
        }
    }
}
