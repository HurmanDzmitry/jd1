package by.hurman_dzmitry.lesson15;


public class Main {
    public static void main(String[] args) throws InterruptedException {
        Customers customers = new Customers();
        CashBox cashBox = new CashBox(customers);
        Store store = new Store(customers);
        CustomersThread customersThread = new CustomersThread(store);
        CashboxThread cashboxThread1 = new CashboxThread(cashBox);
        CashboxThread cashboxThread2 = new CashboxThread(cashBox);
        new Thread(customersThread).start();
        Thread thread1 = new Thread(cashboxThread1);
        Thread thread2 = new Thread(cashboxThread2);
        thread1.start();
        thread2.start();
        Thread.sleep(3000);
        System.out.println(customers.getCustomerList());
    }
}