package by.hurman_dzmitry.lesson15;

import java.util.ArrayList;
import java.util.List;

public class Customers {
    volatile private List<Customer> customerList = new ArrayList<>();

    {
        for (int i = 1; i <= 5; i++) {
            customerList.add(new Customer(i));
        }
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    @Override
    public String toString() {
        return "Customers{" +
                "customerList=" + customerList +
                '}';
    }
}