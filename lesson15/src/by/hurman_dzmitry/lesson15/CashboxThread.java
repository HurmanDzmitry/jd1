package by.hurman_dzmitry.lesson15;

public class CashboxThread implements Runnable {
    CashBox cashBox;

    public CashboxThread(CashBox cashBox) {
        this.cashBox = cashBox;
    }

    @Override
    public void run() {
        cashBox.customersOut();
    }
}
