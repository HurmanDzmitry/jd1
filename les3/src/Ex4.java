public class Ex4 {
    public static void main(String[] args) {
        int x = 1234566;
        int y=x;//отдельно для поиска количества цифр
        int i=0;
        for(int j=0;;j++){
            if(y/10>0){
                y /= 10;
                i++;
            }
            else break;
        }
        System.out.println("Количество цифр в числе = "+(i+1));
        int counter=0;
        for(int j=1;j<Math.pow(10,i)*10;j*=10){
            if((x/j)%(10)-(x/(j*10))%(10)==1)//Math.abs можно с убыванием и возрастанием сразу
                counter++;
        }
        if(counter==(i+1))
            System.out.println(x+" образует последовательность");
        else
            System.out.println(x+" не образует последовательность");
    }
}
