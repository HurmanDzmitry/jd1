public class Ex1 {
    public static void main(String[] args) {
        int block[] = {-5, 2, 87, 4, 5, 44, -99, 106, 15, 74};
        int min = block[0];
        for (int i = 1; i < block.length; i++) {
            if (min > block[i])
                min = block[i];
        }
        System.out.println("Минимальное  число = " + min);
        int max = block[0];
        for (int i = 1; i < block.length; i++) {
            if (max < block[i])
                max = block[i];
        }
        System.out.println("Максимальное  число = " + max);
        System.out.print("Массив после замены = [");
        for (int i = 0; i < block.length; i++) {
            if (block[i] == min)
                block[i] = 0;
            if (block[i] == max)
                block[i] = 99;
            if (i != block.length - 1)
                System.out.print(block[i] + ", ");
            else System.out.print(block[i] + "]");
        }
    }
}