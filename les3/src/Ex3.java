import java.util.Arrays;

public class Ex3 {
    public static void main(String[] args) {
        int block[] = {-5, 2, 87, 4, 5, 44, -99, 106, 15, 74};
        for(int i=0;i<block.length;i++){
            System.out.print(block[i]+" ");
        }
        System.out.println(" ");
        for(int j=0;j<block.length/2;j++){
            int back = block[j];
            block[j] = block[block.length-1-j];
            block[block.length-1-j] = back;
        }
//        for(int i=0;i<block.length;i++){
//            System.out.print(block[i]+" ");
//        }
        System.out.println(Arrays.toString(block));
    }
}
