import java.util.Arrays;
import java.util.Collections;

public class Ex5 {
    public static void main(String[] args) {
        Integer block[] = {-5, 2, -99, 74, 5, 74, -99, 106, 15, 74};
        Arrays.sort(block);
        for(int i=0;i<block.length;i++){
            System.out.print(block[i]+" ");
        }
        System.out.println(" ");
        Arrays.sort(block, Collections.reverseOrder());
        for(int i=0;i<block.length;i++) {
            System.out.print(block[i] + " ");
        }


    }
}
