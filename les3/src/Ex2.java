public class Ex2 {
    public static void main(String[] args) {
        int block[] = {-5, 2, -99, 74, 5, 74, -99, 106, 15, 74};
        for (int i = 0; i < block.length; i++) {
            int counter = 1;
            int repeat = block[i];
            for (int j = i + 1; j < block.length; j++) {
                if (repeat == block[j]) {
                    counter++;
                    block[j] = 0;
                }
            }
            if (counter != 1 && repeat != 0)
                System.out.println("Number " + repeat + " : " + counter + " time");
        }
//        for(int i=0;i<block.length;i++) {   правда меняется массив, нужно создавать копию что ли
//            System.out.print(block[i] + " ");
//        }
    }
}
