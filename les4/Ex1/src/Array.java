import java.util.Arrays;
import java.util.Scanner;

public class Array {
    int[] arr = new int[3];

    public void input() {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan();
        }
    }

    public void print() {
        System.out.println(Arrays.toString(arr));
    }

    public void sort() {
        Arrays.sort(arr);
    }

    public int scan(){
        Scanner num = new Scanner(System.in);
        int number = num.nextInt();
        return number;
    }
}
