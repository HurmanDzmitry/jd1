public class Time {
    private Integer sec;
    private Integer min;
    private Integer hour;


    public Time(int sec) {
        System.out.println("Введи число");
        this.sec = sec;
    }

    public Time(Integer sec, Integer min, Integer hour) {
        System.out.println("Введи число");
        this.sec = sec;
        System.out.println("Введи число");
        this.min = min;
        System.out.println("Введи число");
        this.hour = hour;
    }

    public void transfer() {
        hour = sec / 3600;
        min = (sec - hour * 3600) / 60;
        sec = sec - hour * 3600 - min * 60;
    }

    public Integer transferInSeconds() {
        return sec = hour * 3600 + min * 60 + sec;
    }

    public void output() {
        System.out.println(hour + ":" + min + ":" + sec);
    }

    public void compare(Integer x, Integer y) {
        int z = x.compareTo(y);
        if (z == 0)
            System.out.println("Промежутки равны");
        else if (z > 0)
            System.out.println("Первый промежуток больше");
        else
            System.out.println("Второй промежуток больше");
    }
}