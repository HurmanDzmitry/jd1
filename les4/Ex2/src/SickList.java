import java.util.Arrays;
import java.util.Scanner;

public class SickList {
    Scanner SCAN = new Scanner(System.in);

    Sick[] allSick;
    int size;

    public SickList(int size) {
        allSick = new Sick[size];
        this.size = size;
    }

    public void inPut() {
        for (int i = 0; i < size; i++) {
            System.out.println("Введи имя");
            String name = SCAN.next();
            System.out.println("Введи возраст");
            int age = SCAN.nextInt();
            System.out.println("Болен ли (true/false)");
            boolean illness = SCAN.nextBoolean();
            allSick[i] = new Sick(name, age, illness);
        }
    }

    public void outPut() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return Arrays.toString(allSick);
    }

    public void search(String name) {
        for (int i = 0; i < size; i++) {
            if (name.equals(allSick[i].getName()))
                System.out.println(allSick[i]);
        }
    }

    public void search(int age) {
        for (int i = 0; i < size; i++) {
            if (age == allSick[i].getAge())
                System.out.println(allSick[i]);
        }
    }
}
