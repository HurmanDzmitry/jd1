public class Sick {
    private String name;
    private int age;
    private boolean illness;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Sick(String name, int age, boolean ill) {
        this.name = name;
        this.age = age;
        this.illness = ill;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("Пациент - ").append(name).append("; ");
        out.append("Возраст - ").append(age).append("; ");
        out.append("Болеет ли - ").append(illness).append(".");
        return out.toString();
    }
}