package by.hurman_dzmitry.web;

import by.hurman_dzmitry.web.command.*;
import by.hurman_dzmitry.web.command.add.AddCommand;
import by.hurman_dzmitry.web.command.add.GetAddCommand;
import by.hurman_dzmitry.web.command.add.ListCommand;
import by.hurman_dzmitry.web.command.search.GetSearchingByName;
import by.hurman_dzmitry.web.command.search.ResultSearching;
import by.hurman_dzmitry.web.command.search.SearchingByName;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
public class StocksServlet extends HttpServlet {
    private Map<CommandKey, Command> commands = new HashMap<>();

    {
        commands.put(new CommandKey("GET", "/WebStock_war/stocks"), new ListCommand());
        commands.put(new CommandKey("GET", "/WebStock_war/stocks/add"), new GetAddCommand());
        commands.put(new CommandKey("POST", "/WebStock_war/stocks"), new AddCommand(
                commands.get(new CommandKey("GET", "/WebStock_war/stocks"))
        ));


        commands.put(new CommandKey("GET", "/WebStock_war/stocks/show_search"), new ResultSearching());
        commands.put(new CommandKey("GET", "/WebStock_war/stocks/search"), new GetSearchingByName());
        commands.put(new CommandKey("POST", "/WebStock_war/stocks/show_search"), new SearchingByName(
                commands.get(new CommandKey("GET", "/WebStock_war/stocks/show_search"))
        ));

    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Command command = commands.get(new CommandKey(req.getMethod(), req.getRequestURI()));
        command.execute(req);
        RequestDispatcher dispatcher = getServletContext()
                .getRequestDispatcher("/WEB-INF/main.jsp");
        dispatcher.forward(req, resp);
    }
}