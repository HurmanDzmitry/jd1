package by.hurman_dzmitry.web.command.search;

import by.hurman_dzmitry.domain.Searching;
import by.hurman_dzmitry.web.command.Command;

import javax.servlet.http.HttpServletRequest;

public class ResultSearching implements Command {
    @Override
    public void execute(HttpServletRequest request) {
        request.setAttribute("stocks", Searching.SEARCHING_LIST.getStocks());
        request.setAttribute("page", "/WEB-INF/show_search.jsp");
    }
}
