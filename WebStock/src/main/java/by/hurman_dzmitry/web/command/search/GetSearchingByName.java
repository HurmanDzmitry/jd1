package by.hurman_dzmitry.web.command.search;

import by.hurman_dzmitry.web.command.Command;

import javax.servlet.http.HttpServletRequest;

public class GetSearchingByName implements Command {
    @Override
    public void execute(HttpServletRequest request) {
        request.setAttribute("page", "/WEB-INF/search.jsp");
    }
}
