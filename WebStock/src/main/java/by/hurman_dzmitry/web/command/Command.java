package by.hurman_dzmitry.web.command;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    void execute(HttpServletRequest request);
}