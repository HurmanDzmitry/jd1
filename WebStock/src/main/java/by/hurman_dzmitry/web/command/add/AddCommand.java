package by.hurman_dzmitry.web.command.add;

import by.hurman_dzmitry.domain.Stock;
import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.web.command.Command;

import javax.servlet.http.HttpServletRequest;

public class AddCommand implements Command {
    private Command listCommand;

    public AddCommand(Command listCommand) {
        this.listCommand = listCommand;
    }

    @Override
    public void execute(HttpServletRequest request) {
        StockList.STOCK_LIST.add
                        (new Stock(request.getParameter("name"),
                        Double.valueOf(request.getParameter("bid"))));
        listCommand.execute(request);
    }
}