package by.hurman_dzmitry.web.command.add;

import by.hurman_dzmitry.web.command.Command;

import javax.servlet.http.HttpServletRequest;

public class GetAddCommand implements Command {
    @Override
    public void execute(HttpServletRequest request) {
        request.setAttribute("page", "/WEB-INF/add.jsp");
    }
}