package by.hurman_dzmitry.web.command.add;

import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.web.command.Command;

import javax.servlet.http.HttpServletRequest;

public class ListCommand implements Command {
    @Override
    public void execute(HttpServletRequest request) {
        request.setAttribute("stockList", StockList.STOCK_LIST.getStockList());
        request.setAttribute("page", "/WEB-INF/list.jsp");
    }
}