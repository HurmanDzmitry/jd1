package by.hurman_dzmitry.web.command.search;

import by.hurman_dzmitry.domain.Searching;
import by.hurman_dzmitry.domain.Stock;
import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.web.command.Command;

import javax.servlet.http.HttpServletRequest;

public class SearchingByName implements Command {
    private Command listCommand;

    public SearchingByName(Command listCommand) {
        this.listCommand = listCommand;
    }

    @Override
    public void execute(HttpServletRequest request) {
        Searching.SEARCHING_LIST.getStocks().clear();
        for (Stock stock : StockList.STOCK_LIST.getStockList()) {
            if (stock.getName().equals(request.getParameter("nameForSearching"))) {
                Searching.SEARCHING_LIST.add(stock);
            }
        }
        listCommand.execute(request);
    }
}
