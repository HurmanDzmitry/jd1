package by.hurman_dzmitry.db.h2;

public class StockFindException extends Exception {
    public StockFindException(Throwable cause) {
        super(cause);
    }
}