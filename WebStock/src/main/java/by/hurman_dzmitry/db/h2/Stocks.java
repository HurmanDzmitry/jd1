package by.hurman_dzmitry.db.h2;

import by.hurman_dzmitry.domain.Stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Stocks {
    private DataSource ds = DataSource.getInstance();

    public List<Stock> addStockIntoList() throws StockFindException, ClassNotFoundException {
        Class.forName("org.h2.Driver");
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            List<Stock> stocks = new ArrayList<>();
            connection = ds.getConnection();
            pstmt = connection.prepareStatement(
                    "select * from stock");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Stock stock = new Stock(
                        rs.getString(2),
                        rs.getDouble(3)
                );
                stocks.add(stock);
            }
            return stocks;
        } catch (SQLException e) {
            throw new StockFindException(e);
        } finally {
            try {
                if (rs != null) rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (pstmt != null) pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (connection != null) connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}