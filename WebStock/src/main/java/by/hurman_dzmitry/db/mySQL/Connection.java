package by.hurman_dzmitry.db.mySQL;

import by.hurman_dzmitry.domain.Stock;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Connection {
    String user = "root";
    String password = "1234";
    String url = "jdbc:mysql://localhost:3306/stock?serverTimezone=UTC";

    public List<Stock> setConnection() throws StocksFindException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        List<Stock> stocks;
        try (java.sql.Connection connection = DriverManager.getConnection(url, user, password)) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from stock");
            stocks = new ArrayList<>();
            while (resultSet.next()) {
                Stock stock = new Stock(resultSet.getString(2), resultSet.getDouble(3));
                stocks.add(stock);
            }
        } catch (SQLException ex) {
            throw new StocksFindException(ex);
        }
        return stocks;
    }
}