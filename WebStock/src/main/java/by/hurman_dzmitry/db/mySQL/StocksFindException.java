package by.hurman_dzmitry.db.mySQL;

public class StocksFindException extends Exception {
    public StocksFindException(Throwable cause) {
        super(cause);
    }
}