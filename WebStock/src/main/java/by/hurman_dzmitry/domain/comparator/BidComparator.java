package by.hurman_dzmitry.domain.comparator;

import by.hurman_dzmitry.domain.Stock;

import java.util.Comparator;

public class BidComparator implements Comparator<Stock> {
    @Override
    public int compare(Stock o1, Stock o2) {
        if (o1.getBid() > o2.getBid()) {
            return 1;
        } else if (o1.getBid() < o2.getBid()) {
            return -1;
        } else {
            return 0;
        }
    }
}
