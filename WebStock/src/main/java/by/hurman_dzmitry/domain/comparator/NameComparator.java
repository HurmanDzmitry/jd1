package by.hurman_dzmitry.domain.comparator;

import by.hurman_dzmitry.domain.Stock;

import java.util.Comparator;

public class NameComparator implements Comparator<Stock> {

    @Override
    public int compare(Stock o1, Stock o2) {
        return o1.getName().compareTo(o2.getName());
    }
}