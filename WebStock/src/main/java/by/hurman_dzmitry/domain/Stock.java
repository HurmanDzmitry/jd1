package by.hurman_dzmitry.domain;

public class Stock {
    private String name;
    private double bid;

    public Stock(String name, double bid) {
        this.name = name;
        this.bid = bid;
    }

    public String getName() {
        return name;
    }

    public double getBid() {
        return bid;
    }

    @Override
    public String toString() {
        return "name = " + name +
                ", bid = " + bid;
    }
}
