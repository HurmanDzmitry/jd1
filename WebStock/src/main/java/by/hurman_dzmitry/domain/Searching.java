package by.hurman_dzmitry.domain;

import java.util.ArrayList;
import java.util.List;

public class Searching {
    public static final Searching SEARCHING_LIST = new Searching();
    private List<Stock> stocks = new ArrayList<>();

    private Searching() {
    }

    public Searching add(Stock stock) {
        stocks.add(stock);
        return this;
    }

    public List<Stock> getStocks() {
        return stocks;
    }

    @Override
    public String toString() {
        return "StockList{" +
                "stocks=" + stocks +
                '}';
    }
}