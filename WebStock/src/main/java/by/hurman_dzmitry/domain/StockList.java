package by.hurman_dzmitry.domain;

import by.hurman_dzmitry.db.h2.StockFindException;
import by.hurman_dzmitry.db.h2.Stocks;

import java.util.ArrayList;
import java.util.List;

public class StockList {
    public static final StockList STOCK_LIST = new StockList();
    private List<Stock> stockList = new ArrayList<>();{
        try {
            stockList.addAll(new Stocks().addStockIntoList());
        } catch (StockFindException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private StockList() {
    }

    public StockList add(Stock stock) {
        stockList.add(stock);
        return this;
    }

    public List<Stock> getStockList() {
        return stockList;
    }

    @Override
    public String toString() {
        return "StockList{" +
                "stockList=" + stockList +
                '}';
    }
}