package by.it_academy.lesson8.Menu;

import by.it_academy.lesson8.Text.Operation;

public class MenuNewSentence implements ItemMenu {
    private Operation text;

    public MenuNewSentence(Operation text) {
        this.text = text;
    }

    @Override
    public void execute() {
        text.newSentence();
    }

    @Override
    public String name() {
        return "Ввод новой строки";
    }
}


