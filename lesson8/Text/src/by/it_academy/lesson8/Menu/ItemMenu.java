package by.it_academy.lesson8.Menu;

public interface ItemMenu {
    void execute();

    String name();
}
