package by.it_academy.lesson8.Menu;

import by.it_academy.lesson8.Text.Operation;

public class MenuShowAll implements ItemMenu {
    private MenuRoot rootMenuItem;
    private Operation text;

    public MenuShowAll(Operation text, MenuRoot menuRoot) {
        this.text = text;
        this.rootMenuItem = menuRoot;
    }

    @Override
    public void execute() {
        System.out.print(text.outPut());
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Вывод всех слов";
    }
}

