package by.it_academy.lesson8.Menu;

import by.it_academy.lesson8.Text.Operation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Display implements MenuRoot {
    private static final Scanner SCANNER = new Scanner(System.in);

    public ItemMenu[] subMenus = new ItemMenu[]{
            new MenuShowAll(new Operation(), this),
            new MenuSearchWord(new Operation(), this),
            new MenuNewSentence(new Operation())
    };

    @Override
    public void execute() {
        for (int i = 1; i <= subMenus.length; i++) {
            System.out.println(i + " " + subMenus[i - 1].name());
        }
        try {
            subMenus[SCANNER.nextInt() - 1].execute();
        } catch (InputMismatchException ex) {
            System.out.println("Что-то не так, попробуй еще");
            SCANNER.next();
            this.execute();
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Что-то не так, попробуй еще");
            this.execute();
        }
    }
}