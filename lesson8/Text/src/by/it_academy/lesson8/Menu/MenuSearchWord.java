package by.it_academy.lesson8.Menu;

import by.it_academy.lesson8.Text.Operation;

public class MenuSearchWord implements ItemMenu {
    private MenuRoot rootMenuItem;
    private Operation text;

    public MenuSearchWord(Operation text, MenuRoot menuRoot) {
        this.text = text;
        this.rootMenuItem = menuRoot;
    }

    @Override
    public void execute() {
        System.out.print(text.searchWord());
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Поиск слов с выводом количества повторений";
    }
}

