package by.it_academy.lesson8.Text;

import java.util.*;

public class Text {
    private String myText;
    private static List<String> wordList = new ArrayList<>();
    private Set<String> setWordList = new TreeSet<>(new CompareWord());
    private Map<String, Integer> mapWord = new HashMap<>();

    public Text() {
        myText = giveDesiredText();
        wordDivision();
        addInSet();
        putInMap();
    }

    public Text(String myText) {
       this.myText = myText;
        wordDivision();
        addInSet();
        putInMap();
    }

    public void putInMap() {
        for (int i = 0; i < wordList.size(); i++) {
            int counter = 0;
            for (String aWordList : wordList) {
                if (wordList.get(i).equals(aWordList)) {
                    counter++;
                }
            }
            mapWord.put(wordList.get(i), counter);
        }
    }

    public void addInSet() {
        setWordList.addAll(wordList);
    }

    public void wordDivision() {
        while (myText.contains(" ")) {
            int index = myText.indexOf(" ");
            wordList.add(myText.substring(0, index));
            myText = myText.substring(index + 1);
        }
        wordList.add(myText);
    }

    public Set<String> getSetWordList() {
        return setWordList;
    }

    public Map<String, Integer> getMapWord() {
        return mapWord;
    }

    public List<String> getWordList() {
        return wordList;
    }

    public String giveDesiredText() {
        StringBuilder out = new StringBuilder();
        for (String str : getWordList()) {
            out.append(str).append(" ");
        }
        return out.toString();
    }
}