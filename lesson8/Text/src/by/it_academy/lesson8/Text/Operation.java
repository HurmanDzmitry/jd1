package by.it_academy.lesson8.Text;

import by.it_academy.lesson8.Menu.Display;

import java.util.Map;
import java.util.Scanner;

public class Operation extends Text {
    private static final Scanner SCANNER = new Scanner(System.in);

    public String outPut() {
        StringBuilder out = new StringBuilder();
        out.append("Слова в предложении:");
        for (String str : getSetWordList()) {
            out.append(str).append('\n');
        }
        return out.toString();
    }

    public String searchWord() {
        StringBuilder out = new StringBuilder();
        System.out.println("Введи слово:");
        String text = SCANNER.next();
        for (Map.Entry<String, Integer> stringIntegerEntry : getMapWord().entrySet()) {
            String key = stringIntegerEntry.getKey();
            if (text.equals(key)) {
                out.append(key).append(" - ").append(getMapWord().get(key) / 4).append(" раз").append('\n');
            }
        }
        if (out.toString().length() == 0)
            return "Таких нет" + '\n';
        else
            return out.toString();
    }

    public void newSentence() {
        getWordList().clear();
        System.out.println("Введи предложение");
        new Text(SCANNER.nextLine());
        new Display().execute();
    }
}
