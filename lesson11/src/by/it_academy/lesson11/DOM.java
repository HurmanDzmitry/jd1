package by.it_academy.lesson11;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;

public class DOM implements inStream {
    public void inStream() {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse("patient.xml");
            Node root = document.getDocumentElement();
            NodeList patientList = root.getChildNodes();
            for (int i = 0; i < patientList.getLength(); i++) {
                Node patient = patientList.item(i);
                if (patient.getNodeType() != Node.TEXT_NODE) {
                    NodeList patientChildList = patient.getChildNodes();
                    for (int j = 0; j < patientChildList.getLength(); j++) {
                        Node childPatient = patientChildList.item(j);
                        if (childPatient.getNodeType() != Node.TEXT_NODE) {
                            System.out.println(childPatient.getNodeName() + ":" + childPatient.getChildNodes().item(0).getTextContent());
                        }
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void outStream() throws TransformerFactoryConfigurationError, DOMException {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse("patient.xml");
            Node root = document.getDocumentElement();
            for (Patient patient : PatientList.getPatients()) {
                Element p = document.createElement("Patient");
                Element firstName = document.createElement("FirstName");
                firstName.setTextContent(patient.getFirstName());
                Element lastName = document.createElement("LastName");
                lastName.setTextContent(patient.getLastName());
                Element getBirth = document.createElement("Birth");
                getBirth.setTextContent(patient.getBirth());
                Element alive = document.createElement("Alive");
                alive.setTextContent(String.valueOf(patient.isAlive()));
                p.appendChild(firstName);
                p.appendChild(lastName);
                p.appendChild(getBirth);
                p.appendChild(alive);
                root.appendChild(p);
            }
            // Записываем XML в файл
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);
            FileOutputStream fos = new FileOutputStream("patient.xml");
            StreamResult result = new StreamResult(fos);
            tr.transform(source, result);
        } catch (TransformerException | IOException | ParserConfigurationException | SAXException ex) {
            System.out.println(ex.getMessage());
        }
    }
}