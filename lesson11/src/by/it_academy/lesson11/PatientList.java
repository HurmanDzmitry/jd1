package by.it_academy.lesson11;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PatientList {
    public static final Scanner SCANNER = new Scanner(System.in);

    public static List<Patient> getPatients() {
        return patients;
    }

    static List<Patient> patients = new ArrayList<>();

    {
        patients.add(new Patient("Paul", "Mccartney", 1942, 6, 18, true));
        patients.add(new Patient("Michael", "Jackson", 1958, 8, 29, false));
    }

    public void newPatient() {
        patients.add(new Patient(SCANNER.next(),
                SCANNER.next(),
                SCANNER.nextInt(),
                SCANNER.nextInt(),
                SCANNER.nextInt(),
                SCANNER.nextBoolean()));
    }
}