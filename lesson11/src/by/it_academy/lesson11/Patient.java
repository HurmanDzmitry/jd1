package by.it_academy.lesson11;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Patient {
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    private String firstName;
    private String lastName;
    private Date birth;
    private boolean alive;

    public Patient(String firstName, String lastName, int year, int month, int day, boolean alive) {
        this.firstName = firstName;
        this.lastName = lastName;
        GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
        birth = calendar.getTime();
        this.alive = alive;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirth() {
        return SIMPLE_DATE_FORMAT.format(birth.getTime());
    }

    public boolean isAlive() {
        return alive;
    }

    @Override
    public String toString() {
        return firstName + ";" + lastName + ";" + SIMPLE_DATE_FORMAT.format(birth.getTime()) + ";" + alive;
    }
}
