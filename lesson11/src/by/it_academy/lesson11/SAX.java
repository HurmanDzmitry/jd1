package by.it_academy.lesson11;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAX implements inStream {
    @Override
    public void inStream() {
        final String fileName = "patient.xml";
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DefaultHandler handler = new DefaultHandler() {
                boolean FirstName = false;
                boolean LastName = false;
                boolean Birth = false;
                boolean Alive = false;

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    if (qName.equalsIgnoreCase("FirstName"))
                        FirstName = true;
                    if (qName.equalsIgnoreCase("LastName"))
                        LastName = true;
                    if (qName.equalsIgnoreCase("Birth"))
                        Birth = true;
                    if (qName.equalsIgnoreCase("Alive"))
                        Alive = true;

                }

                @Override
                public void characters(char ch[], int start, int length) throws SAXException {
                    if (FirstName) {
                        System.out.println("FirstName: " + new String(ch, start, length));
                        FirstName = false;
                    }
                    if (LastName) {
                        System.out.println("LastName: " + new String(ch, start, length));
                        LastName = false;
                    }
                    if (Birth) {
                        System.out.println("Birth: " + new String(ch, start, length));
                        Birth = false;
                    }
                    if (Alive) {
                        System.out.println("Alive: " + new String(ch, start, length));
                        Alive = false;
                    }
                }
            };
            saxParser.parse(fileName, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}