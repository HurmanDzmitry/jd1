package by.it_academy.lesson11;

import java.io.*;

public class FileStream implements inStream {

    public void outStream() {
        try (
                DataOutputStream dos = new DataOutputStream(new FileOutputStream("patient.txt"))) {
            for (Patient patient : PatientList.getPatients()) {
                dos.writeUTF(patient.getFirstName());
                dos.writeUTF(patient.getLastName());
                dos.writeUTF(patient.getBirth());
                dos.writeBoolean(patient.isAlive());
            }
        } catch (
                IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    public void inStream() {
        try (
                DataInputStream dos = new DataInputStream(new FileInputStream("patient.txt"))) {
            for (int i = 0; i < PatientList.getPatients().size(); i++) {
                String firstName = dos.readUTF();
                String lastName = dos.readUTF();
                String birth = dos.readUTF();
                boolean isAlive = dos.readBoolean();
                System.out.printf("%s;%s;%s;%b\n", firstName, lastName, birth, isAlive);
            }
        } catch (
                IOException ex) {

            System.out.println(ex.getMessage());
        }
    }
}
