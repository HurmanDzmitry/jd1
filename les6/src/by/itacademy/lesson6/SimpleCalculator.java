package by.itacademy.lesson6;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsBoundsException;
import by.itacademy.lesson6.operations.Addition;
import by.itacademy.lesson6.operations.Division;
import by.itacademy.lesson6.operations.Exhibitor;
import by.itacademy.lesson6.operations.Multiply;
import by.itacademy.lesson6.operations.Operation;
import by.itacademy.lesson6.operations.SquareRoot;
import by.itacademy.lesson6.operations.Subtraction;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SimpleCalculator {
    public static final Logger LOGGER = Logger.getLogger(SimpleCalculator.class.getName());

    private Operation[] operations = {
            new Addition(),
            new Division(),
            new Exhibitor(),
            new Multiply(),
            new SquareRoot(),
            new Subtraction()
    };

    public void execute(int index, Operands operands) {
        try {
            System.out.println(operations[index].calculate(operands));
        } catch (OperandsBoundsException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }
}
