package by.itacademy.lesson6;

import by.itacademy.lesson6.operands.OperandsTwo;

public class Main {
    public static void main(String[] args) {
        SimpleCalculator calculator = new SimpleCalculator();
        calculator.execute(0, new OperandsTwo(10, 75));

    }
}