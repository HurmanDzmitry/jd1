package by.itacademy.lesson7;

public class Furniture {
    private int squareFurniture;
    private String name;

    public Furniture(String name, int square) {
        this.squareFurniture = square;
        this.name = name;
    }

    public int getSquareFurniture() {
        return squareFurniture;
    }

    public String getName() {
        return name;
    }
}
