package by.itacademy.lesson7;

import java.util.ArrayList;
import java.util.List;

public class Room {
    private int indexRoom, squareRoom, window;
    private List<Lightbulb> lightbulbList = new ArrayList<>();
    private List<Furniture> furnitureList = new ArrayList<>();

    public Room(int index, int square, int window) {
        this.indexRoom = index;
        this.squareRoom = square;
        this.window = window;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        try {
            out.append(" Комната " + getIndexRoom() + '\n' +
                    "  Освещенность = " + generalLight() +
                    '(' + getWindow() + " окна по 700 лк, лампочки: " + lightBulb() + ')' + '\n' +
                    "  Площадь = " + getSquareRoom() + " м^2,(занято " + squareOfFurniture() + " м^2, свободно " +
                    (getSquareRoom() - squareOfFurniture()) +
                    " м^2 или " + furniturePercentage() + "% от площади)" + '\n' +
                    "  Мебель:\n" + "   " + furniture());
        } catch (IlluminanceLimitException | SpaceLimitException e) {
            e.printStackTrace();
        }
        return out.toString();
    }

    private int furniturePercentage() throws SpaceLimitException {
        int furniturePercentage = (getSquareRoom() - squareOfFurniture()) * 100 / getSquareRoom();
        if (furniturePercentage < 30) {
            throw new SpaceLimitException("мало места");
        }
        return furniturePercentage;
    }

    private int generalLight() throws IlluminanceLimitException {
        int generallight = getWindow() * 700 + lightOfBulb();
        if (generallight < 300 || generallight > 4000) {
            throw new IlluminanceLimitException("light is very bad");
        }
        return generallight;
    }

    private String furniture() {
        StringBuilder out = new StringBuilder();
        for (Furniture furniture : furnitureList) {
            out.append(furniture.getName()).append(" (площадь ")
                    .append(furniture.getSquareFurniture())
                    .append(" м^2)").append('\n').append("   ");
        }
        return out.toString();
    }

    private int squareOfFurniture() {
        int square = 0;
        for (Furniture furniture : furnitureList) {
            square += furniture.getSquareFurniture();
        }
        return square;
    }

    private int lightOfBulb() {
        int light = 0;
        for (Lightbulb lightbulb : lightbulbList) {
            light += lightbulb.getBrightness();
        }
        return light;
    }

    private String lightBulb() {
        StringBuilder out = new StringBuilder();
        for (Lightbulb lightbulb : lightbulbList) {
            out.append(lightbulb.getBrightness()).append(" лк, ");
        }
        return out.toString();
    }

    public int getIndexRoom() {
        return indexRoom;
    }

    public int getSquareRoom() {
        return squareRoom;
    }

    public int getWindow() {
        return window;
    }

    public void addLightbulb(Lightbulb lightBulb) {
        lightbulbList.add(lightBulb);
    }

    public void addFurniture(Furniture furniture) {
        furnitureList.add(furniture);
    }
}