package by.itacademy.lesson7;

public class IlluminanceLimitException extends Exception {
    IlluminanceLimitException(String message) {
        super(message);
    }
}
