package by.itacademy.lesson7;

import java.util.ArrayList;
import java.util.List;

public class Building {
    private int index;
    private List<Room> roomList = new ArrayList<>();

    @Override
    public String toString() {
        return "Здание " + index + '\n' + room();
    }

    private String room() {
        StringBuilder out = new StringBuilder();
        for (Room room : roomList) {
            out.append(room);
        }
        return out.toString();
    }

    public Building(int index) {
        this.index = index;
    }

    public void addRoom(int indexRoom, int squareRoom, int window) {
        roomList.add(new Room(indexRoom, squareRoom, window));
    }

    public void addLightbulb(int indexRoom, Lightbulb lightbulb) {
        roomList.get(indexRoom).addLightbulb(lightbulb);
    }

    public void addFurniture(int indexRoom, Furniture furniture) {
        roomList.get(indexRoom).addFurniture(furniture);
    }

    public void info() {
        System.out.println(this);
    }

}