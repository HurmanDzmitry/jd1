package by.itacademy.lesson7;

public class SpaceLimitException extends Exception {
    SpaceLimitException(String message) {
        super(message);
    }
}