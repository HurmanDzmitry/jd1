package by.itacademy.lesson7;

public class Main {
    public static void main(String[] args) {
        Building building = new Building(1);
        building.addRoom(1, 100, 3);
        building.addRoom(2, 5, 2);
        building.addLightbulb(0, new Lightbulb(150));
        building.addLightbulb(0, new Lightbulb(250));
        building.addFurniture(0, new Furniture("Стол", 3));
        building.addFurniture(0, new Furniture("Кресло", 10));
        building.info();
    }
}