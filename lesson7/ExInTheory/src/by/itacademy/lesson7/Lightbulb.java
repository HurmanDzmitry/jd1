package by.itacademy.lesson7;

public class Lightbulb {
    private int brightness;

    public Lightbulb(int brightness) {
        this.brightness = brightness;
    }

    public int getBrightness() {
        return brightness;
    }
}
