package by.itacademy.lesson7;

import java.util.Iterator;

public class ArrayList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private T[] array;
    private int size = 0;

    public ArrayList() {
        array = (T[]) new Object[DEFAULT_CAPACITY];
    }

    public T[] grow() {
        T[] newArray = (T[]) new Object[array.length + DEFAULT_CAPACITY];
        System.arraycopy(array, 0, newArray, 0, array.length);
        array = newArray;
        return array;
    }

    @Override
    public void add(T element) {
        if (size >= array.length) {
            array = grow();
        }
        array[size] = element;
        size++;
    }

    public void add(T element, int index) {
        if ((size) >= array.length) {
            array = grow();
        }
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = element;
        size++;
    }

    @Override
    public boolean remove(int index) {
        System.arraycopy(array, index + 1, array, index, size - index);
        size--;
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    public T get(int index) {
        return array[index];
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return (array[currentIndex]!=null);
            }

            @Override
            public T next()  {
                return array[currentIndex++];
            }
        };
    }
}
