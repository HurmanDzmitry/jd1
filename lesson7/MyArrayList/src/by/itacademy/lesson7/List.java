package by.itacademy.lesson7;

public interface List<T> extends Iterable<T>{
    void add(T element);
    int size();
    boolean remove(int index);
    T get(int index);
}