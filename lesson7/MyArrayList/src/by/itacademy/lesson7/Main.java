package by.itacademy.lesson7;

public class Main {
    public static void main(String[] args) {
//        ArrayList<Integer> arrayList = new ArrayList<Integer>();
////        arrayList.add(12);
////        arrayList.add(77);
////        arrayList.add(66, 1);
////        arrayList.remove(0);
////        System.out.println(arrayList.size());
////        System.out.println(arrayList.get(1));
////        for (Object o : arrayList) {
////            System.out.println(o);
////        }
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        linkedList.add(12);
        linkedList.add(77);
        linkedList.add(33);
        linkedList.remove(1);
//        System.out.println(linkedList.get(1));
        for (Object o : linkedList) {
            System.out.println(o);
        }
    }
}