package by.itacademy.lesson7;

import java.util.Iterator;

public class LinkedList<T> implements List<T> {
    private int size = 0;
    private Node<T> first;
    private Node<T> last;


    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return (currentIndex < size && get(currentIndex) != null);
            }

            @Override
            public T next() {
                return get(currentIndex++);
            }
        };
    }

    @Override
    public void add(T element) {
        final Node<T> l = last;
        final Node<T> newNode = new Node<>(l, element, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean remove(int index) {
        boolean result = false;
        Node prev = first;
        Node curr = first;
        while (curr.next != null || curr == last) {
            if (curr.item.equals(get(index))) {
                if (size == 1) { first = null; last = null; }
                else if (curr.equals(first)) { first = first.next; }
                else if (curr.equals(last)) { last = prev; last.next = null; }
                else { prev.next = curr.next; }
                size--;
                result = true;
                break;
            }
            prev = curr;
            curr = prev.next;
        }
        return result;
    }

    @Override
    public T get(int index) {
        Node<T> element = first;
        for (int i = 0; i < index; i++) {
            element = element.next;
        }
        return element.item;
    }


    private static class Node<T> {
        T item;
        Node<T> next;
        Node<T> prev;

        Node(Node<T> prev, T element, Node<T> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
}
