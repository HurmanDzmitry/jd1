package by.hurman_dzmitry.operations;

import by.hurman_dzmitry.domain.Stock;
import by.hurman_dzmitry.domain.StockList;
import org.junit.Assert;
import org.junit.Test;

public class maxDifferencePriceTest {
    @Test
    public void maxDifferencePriceShouldBeValid() {
        StockList stockList = new StockList();
        stockList.add(new Stock("Башнефть, ao", 1791, 1773, 1805, false));
        stockList.add(new Stock("Polymetal Int., ao", 553.5, 553, 555, true));
        stockList.add(new Stock("АВТОВАЗ, ао", 9.7900, 9.700, 9.700, true));

        String result = stockList.maxDifferencePrice();

        String expectedResult = "Башнефть - разница в 1,805 %\n";

        Assert.assertEquals(result, expectedResult);
    }
}
