package by.hurman_dzmitry.domain;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Searching extends StockList {
    private static final Scanner SCANNER = new Scanner(System.in);

    public String searchByName() {
        StringBuilder out = new StringBuilder();
        Pattern p = Pattern.compile(".*" + SCANNER.next() + ".*");
        String text;
        for (Stock aStockList : stockList) {
            text = aStockList.getName().replace(", ao", "");
            Matcher m = p.matcher(text);
            if (m.find()) {
                out.append(aStockList).append("\n");
            }
        }
        return out.toString().length() == 0 ? "Таких нет\n" : out.toString();
    }

    public String searchByVisible() {
        StringBuilder out = new StringBuilder();
        for (Stock aStockList : stockList) {
            if (aStockList.isVisible()) {
                out.append(aStockList).append("\n");
            }
        }
        return out.toString();
    }
}
