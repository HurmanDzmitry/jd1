package by.hurman_dzmitry.domain;

import by.hurman_dzmitry.domain.comparator.BidComparator;
import by.hurman_dzmitry.domain.comparator.NameComparator;
import by.hurman_dzmitry.db.StockFindException;
import by.hurman_dzmitry.db.Stocks;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class StockList {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static List<Stock> stockList = new ArrayList<>();

    public static List<Stock> getStockList() {
        return stockList;
    }

    public void create() throws StockFindException {

        stockList = new Stocks().addStockIntoList();
    }

    public void add(Stock stock) {
        stockList.add(stock);
    }

    public Stock helperForAdd() {
        System.out.println("Bведите имя: ");
        String name = SCANNER.nextLine();
        System.out.println("Bведите предлогаемую стоимость: ");
        double bid = SCANNER.nextDouble();
        System.out.println("Bведите минимальную стоимость: ");
        double minPrice = SCANNER.nextDouble();
        System.out.println("Bведите максимальную стоимость: ");
        double maxPrice = SCANNER.nextDouble();
        System.out.println("Bведите есть ли в наличии (false/true): ");
        boolean visible = SCANNER.nextBoolean();
        SCANNER.nextLine();
        return new Stock(name, bid, minPrice, maxPrice, visible);
    }

    public String outPut() {
        stockList.sort(new NameComparator());
        StringBuilder out = new StringBuilder();
        for (Stock stock : stockList) {
            out.append(stock).append("\n");
        }
        return out.toString();
    }

    public String sortByBid() {
        stockList.sort(new BidComparator());
        return outPut();
    }

    public String averagePrice() {
        StringBuilder out = new StringBuilder();
        for (Stock aStockList : stockList) {
            out.append(aStockList.getName())
                    .append(" ").append((((aStockList.getMaxPrice()) + aStockList.getMinPrice())) / 2)
                    .append("\n");
        }
        return out.toString();
    }

    public String maxDifferencePrice() {
        double[] array = new double[stockList.size()];
        for (int i = 0; i < stockList.size(); i++) {
            array[i] = (stockList.get(i).getMaxPrice() - stockList.get(i).getMinPrice()) / stockList.get(i).getMinPrice() * 100;
        }
        Arrays.sort(array);
        double maxDifference = array[array.length - 1];
        String out = "";
        for (Stock aStockList : stockList) {
            if (maxDifference == (aStockList.getMaxPrice() - aStockList.getMinPrice()) / aStockList.getMinPrice() * 100)
                out = String.format("%s%s%.3f%s", aStockList.getName().substring(0, aStockList.getName().indexOf(',')),
                        " - разница в ", maxDifference, " %" + '\n');
        }
        return out;
    }

    public String readUsingBufferedReader(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }

    public void toJson() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonFileWriter();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void jsonFileWriter() throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        String json = gson.toJson(stockList);
        FileWriter file = new FileWriter("src\\main\\resources\\stocks.json");
        file.write(json);
        file.flush();
        file.close();
    }

    public void fromJson() throws IOException {
        String json = readUsingBufferedReader("src\\main\\resources\\stocks.json");
        Stock[] targetArray = new GsonBuilder().create().fromJson(json, Stock[].class);
        stockList = new ArrayList<>(Arrays.asList(targetArray));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockList stockList1 = (StockList) o;
        return Objects.equals(stockList, stockList1.stockList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockList);
    }
}