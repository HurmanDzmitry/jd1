package by.hurman_dzmitry.domain;

public class Stock {
    private String name;
    private double bid, minPrice, maxPrice;
    private boolean visible;

    public double getBid() {
        return bid;
    }

    public String getName() {
        return name;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public boolean isVisible() {
        return visible;
    }

    public Stock(String name, double bid, double minPrice, double maxPrice, boolean visible) {
        this.name = name;
        this.bid = bid;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.visible = visible;
    }

    @Override
    public String toString() {
        return "name = " + name +
                ", bid = " + bid +
                ", minPrice = " + minPrice +
                ", maxPrice = " + maxPrice +
                ", visible = " + visible;
    }
}
