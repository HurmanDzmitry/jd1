package by.hurman_dzmitry;

import by.hurman_dzmitry.db.StockFindException;
import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.menu.MenuDisplay;

public class Main {
    public static void main(String[] args) throws StockFindException {
        new StockList().create();
        new MenuDisplay().execute();
    }
}
