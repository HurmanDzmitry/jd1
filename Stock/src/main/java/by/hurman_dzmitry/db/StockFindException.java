package by.hurman_dzmitry.db;

public class StockFindException extends Exception {
    public StockFindException(Throwable cause) {
        super(cause);
    }
}