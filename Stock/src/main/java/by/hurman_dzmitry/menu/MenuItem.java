package by.hurman_dzmitry.menu;

public interface MenuItem {
    void execute();

    String name();
}
