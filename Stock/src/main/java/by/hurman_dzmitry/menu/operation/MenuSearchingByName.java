package by.hurman_dzmitry.menu.operation;

import by.hurman_dzmitry.domain.Searching;
import by.hurman_dzmitry.menu.MenuItem;
import by.hurman_dzmitry.menu.RootMenuItem;

public class MenuSearchingByName implements MenuItem {

    private RootMenuItem rootMenuItem;

    public MenuSearchingByName(RootMenuItem rootMenu) {
        this.rootMenuItem = rootMenu;
    }

    @Override
    public void execute() {
        System.out.println("Введи имя");
        System.out.println(new Searching().searchByName());
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Поиск по имени";
    }
}
