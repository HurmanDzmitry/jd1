package by.hurman_dzmitry.menu.operation;

import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.menu.MenuItem;
import by.hurman_dzmitry.menu.RootMenuItem;

public class MenuAveragePrice implements MenuItem {
    private RootMenuItem rootMenuItem;
    private StockList stockList;

    public MenuAveragePrice(StockList stockList, RootMenuItem rootMenu) {
        this.stockList = stockList;
        this.rootMenuItem = rootMenu;
    }

    @Override
    public void execute() {
        System.out.println(stockList.averagePrice());
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Средняя цена акций";
    }
}
