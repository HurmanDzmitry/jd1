package by.hurman_dzmitry.menu.operation;

import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.menu.MenuItem;
import by.hurman_dzmitry.menu.RootMenuItem;

public class MenuMaxDifferencePrice implements MenuItem {
    private RootMenuItem rootMenuItem;
    private StockList stockList;

    public MenuMaxDifferencePrice(StockList stockList, RootMenuItem rootMenu) {
        this.rootMenuItem = rootMenu;
        this.stockList = stockList;
    }

    @Override
    public void execute() {
        System.out.println(stockList.maxDifferencePrice());
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Компания с максимальной разбежкой между минимальной и максимальной ценой акции";
    }
}
