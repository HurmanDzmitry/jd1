package by.hurman_dzmitry.menu.operation;

import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.menu.MenuItem;
import by.hurman_dzmitry.menu.RootMenuItem;

public class MenuSortByBid implements MenuItem {
    private RootMenuItem rootMenuItem;
    private StockList stockList;

    public MenuSortByBid(StockList stockList, RootMenuItem rootMenu) {
        this.rootMenuItem = rootMenu;
        this.stockList = stockList;
    }

    @Override
    public void execute() {
        System.out.println(stockList.sortByBid());
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Сортировка по предложению";
    }
}
