package by.hurman_dzmitry.menu.operation;

import by.hurman_dzmitry.menu.MenuItem;

public class MenuExit implements MenuItem {
    @Override
    public void execute() {
        System.exit(0);
    }

    public String name() {
        return "Выход";
    }
}
