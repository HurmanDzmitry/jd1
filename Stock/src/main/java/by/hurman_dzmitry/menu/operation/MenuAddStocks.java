package by.hurman_dzmitry.menu.operation;

import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.menu.MenuItem;
import by.hurman_dzmitry.menu.RootMenuItem;

public class MenuAddStocks implements MenuItem {
    private RootMenuItem rootMenuItem;
    private StockList stockList;

    public MenuAddStocks(StockList stockList, RootMenuItem rootMenu) {
        this.rootMenuItem = rootMenu;
        this.stockList = stockList;
    }

    @Override
    public void execute() {
        stockList.add(stockList.helperForAdd());
        stockList.toJson();
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Добавление акции";
    }
}

