package by.hurman_dzmitry.menu.operation;

import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.menu.MenuItem;
import by.hurman_dzmitry.menu.RootMenuItem;

public class MenuShowAll implements MenuItem {
    private RootMenuItem rootMenuItem;
    private StockList stockList;

    public MenuShowAll(StockList stockList, RootMenuItem rootMenu) {
        this.rootMenuItem = rootMenu;
        this.stockList = stockList;
    }

    @Override
    public void execute() {
        System.out.println(stockList.outPut());
        rootMenuItem.execute();
    }

    @Override
    public String name() {
        return "Вывод всех акций";
    }
}
