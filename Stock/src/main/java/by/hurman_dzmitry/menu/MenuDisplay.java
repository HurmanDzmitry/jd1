package by.hurman_dzmitry.menu;

import by.hurman_dzmitry.domain.StockList;
import by.hurman_dzmitry.menu.operation.*;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuDisplay implements RootMenuItem {
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final Logger LOGGER = Logger.getLogger(MenuDisplay.class.getName());

    private MenuItem[] subMenus = new MenuItem[]{
            new MenuShowAll(new StockList(), this),
            new MenuSortByBid(new StockList(), this),
            new MenuSearchingByName(this),
            new MenuSearchingByVisible(this),
            new MenuAveragePrice(new StockList(), this),
            new MenuMaxDifferencePrice(new StockList(), this),
            new MenuAddStocks(new StockList(), this),
            new MenuExit()
    };

    @Override
    public void execute() {
        System.out.println("Выбери операцию");
        for (int i = 1; i <= subMenus.length; i++) {
            System.out.println(i + " " + subMenus[i - 1].name());
        }
        try {
            subMenus[SCANNER.nextInt() - 1].execute();
        } catch (InputMismatchException ex) {
            System.out.println("Что-то не так, попробуй еще!");
            LOGGER.log(Level.WARNING, ex.getMessage(), ex);
            SCANNER.next();
            this.execute();
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Что-то не так, попробуй еще!");
            LOGGER.log(Level.WARNING, ex.getMessage(), ex);
            this.execute();
        }
    }
}